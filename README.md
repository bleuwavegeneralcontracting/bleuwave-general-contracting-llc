General Contractor specializing in school, commercial, industrial, restaurant, office, and mixed-use construction. Construction services include land development, site improvement, preconstruction, design-build, general contracting, remodel and rennovations, and construction management.

Address: 2350 E Germann Rd, Suite 25, Chandler, AZ 85286, USA

Phone: 480-744-0000

Website: http://bleuwave.com/
